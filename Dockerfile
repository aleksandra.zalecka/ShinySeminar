FROM rocker/r-ver:3.4.4

#install and load necessary packages
RUN R -e "install.packages(c('shiny', 'shinythemes', 'ggplot2', 'plyr'), repos = 'http://cran.uk.r-project.org')"

COPY . /usr/local/src/app
WORKDIR /usr/local/src/app

EXPOSE 8080
CMD ["Rscript", "app_run.R"]
